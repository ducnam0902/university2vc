package universityfrontend.model;

public class ScoringStudent {
	String subjectId;
	String subjectName;
	String score1;
	String score2;
	String score3;
	String midScore;
	String averageScore;
	String Condition;
	
	public String getAverageScore() {
		return averageScore;
	}
	public void setAverageScore(String averageScore) {
		this.averageScore = averageScore;
	}
	public String getCondition() {
		return Condition;
	}
	public void setCondition(String condition) {
		Condition = condition;
	}
	public ScoringStudent(String subjectId, String subjectName, String score1, String score2, String score3,
			String midScore, String averageScore, String condition) {
		super();
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.score1 = score1;
		this.score2 = score2;
		this.score3 = score3;
		this.midScore = midScore;
		this.averageScore = averageScore;
		Condition = condition;
	}
	public ScoringStudent() {
		super();
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getScore1() {
		return score1;
	}
	public void setScore1(String score1) {
		this.score1 = score1;
	}
	public String getScore2() {
		return score2;
	}
	public void setScore2(String score2) {
		this.score2 = score2;
	}
	public String getScore3() {
		return score3;
	}
	public void setScore3(String score3) {
		this.score3 = score3;
	}
	public String getMidScore() {
		return midScore;
	}
	public void setMidScore(String midScore) {
		this.midScore = midScore;
	}
	
}
