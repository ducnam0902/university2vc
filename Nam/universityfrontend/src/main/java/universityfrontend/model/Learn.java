package universityfrontend.model;

public class Learn {
	String studentId;
	String subjectId;
	String programId;
	float score_1;
	float score_2;
	float score_3;
	public Learn(String studentId, String subjectId, float score_1, float score_2, float score_3, float mid_score) {
		super();
		this.studentId = studentId;
		this.subjectId = subjectId;
		this.score_1 = score_1;
		this.score_2 = score_2;
		this.score_3 = score_3;
		this.mid_score = mid_score;
	}
	float mid_score;
	public Learn() {
		super();
	}
	public Learn(String studentId, String subjectId, String programId, float score_1, float score_2, float score_3,
			float mid_score) {
		super();
		this.studentId = studentId;
		this.subjectId = subjectId;
		this.programId = programId;
		this.score_1 = score_1;
		this.score_2 = score_2;
		this.score_3 = score_3;
		this.mid_score = mid_score;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public float getScore_1() {
		return score_1;
	}
	public void setScore_1(float score_1) {
		this.score_1 = score_1;
	}
	public float getScore_2() {
		return score_2;
	}
	public void setScore_2(float score_2) {
		this.score_2 = score_2;
	}
	public float getScore_3() {
		return score_3;
	}
	public void setScore_3(float score_3) {
		this.score_3 = score_3;
	}
	public float getMid_score() {
		return mid_score;
	}
	public void setMid_score(float mid_score) {
		this.mid_score = mid_score;
	}

	
}
