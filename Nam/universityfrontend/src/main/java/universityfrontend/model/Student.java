package universityfrontend.model;
public class Student {
	String studentId;
	String studentName;
	int startedYear;
	String address;
	String phoneNumber;
	public Student(String studentId, String studentName, int startedYear, String address, String phoneNumber) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.startedYear = startedYear;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}
	public Student() {
		super();
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public int getStartedYear() {
		return startedYear;
	}
	public void setStartedYear(int startedYear) {
		this.startedYear = startedYear;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}
