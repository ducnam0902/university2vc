package universityfrontend.model;


public class ProgramSubject  {
	String subjectId;
	String subjectName;
	int credit;

	String programId;
	String teacherId;
	public ProgramSubject(String subjectId, String subjectName, int credit, String programId, String teacherId) {
		super();
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.credit = credit;
		this.programId = programId;
		this.teacherId = teacherId;
	}
	public ProgramSubject() {
		super();
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	
}
