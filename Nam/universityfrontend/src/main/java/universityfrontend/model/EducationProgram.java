package universityfrontend.model;

public class EducationProgram {
	String programId;
	String programName;
	int totalCredit;
	String falcutyId;
	public EducationProgram(String programId, String programName, int totalCredit, String falcutyId) {
		super();
		this.programId = programId;
		this.programName = programName;
		this.totalCredit = totalCredit;
		this.falcutyId = falcutyId;
	}
	public EducationProgram() {
		super();
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public int getTotalCredit() {
		return totalCredit;
	}
	public void setTotalCredit(int totalCredit) {
		this.totalCredit = totalCredit;
	}
	public String getFalcutyId() {
		return falcutyId;
	}
	public void setFalcutyId(String falcutyId) {
		this.falcutyId = falcutyId;
	}
}
