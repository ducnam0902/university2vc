package universityfrontend.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import universityfrontend.model.ProgramSubject;

@Controller
@RequestMapping(value="subject")
public class SubjectController {
	private RestTemplate rest = new RestTemplate();
	@GetMapping
	public String getAllSubject(Model model) {
		List<ProgramSubject> subject = Arrays.asList(rest.getForObject("http://localhost:8080/subject", ProgramSubject[].class));
		model.addAttribute("subject", subject);
		return "Subject";
	}
	@GetMapping(value="/add")
	public String addNewSubject(Model model) {
		List<String> programId = Arrays.asList(rest.getForObject("http://localhost:8080/edu/listProgramId", String[].class));
		model.addAttribute("programId", programId);
		List<String> teacherId = Arrays.asList(rest.getForObject("http://localhost:8080/teacher/listTeacherId", String[].class));
		model.addAttribute("teacherId", teacherId);
		return "AddSubject";
	}
	@PostMapping(value="/add")
	public String addNewSubject(@RequestParam("subjectId")String subjectId,@RequestParam("subjectName")String subjectName,@RequestParam("credit")String credit,
			@RequestParam("programId")String programId,@RequestParam("teacherId")String teacherId) {
		ProgramSubject s = new ProgramSubject(subjectId,subjectName,Integer.parseInt(credit),programId,teacherId);
		rest.postForObject("http://localhost:8080/subject",s, ProgramSubject.class);
		return "redirect:/subject";
		
	}
	@GetMapping(value="/delete/{id1}/{id2}")
	public String deleteSubject(@PathVariable("id1")String id1,@PathVariable("id2")String id2) {
		rest.delete("http://localhost:8080/subject/delete/{id1}/{id2}",id1,id2);
		return "redirect:/subject";
	}
	@GetMapping(value="/edit/{id1}/{id2}")
	public String editSubject(@PathVariable("id1")String id1,@PathVariable("id2")String id2,Model model) {
		ProgramSubject subject= rest.getForObject("http://localhost:8080/subject/edit/{id1}/{id2}",ProgramSubject.class,id1,id2);
		List<String> programId = Arrays.asList(rest.getForObject("http://localhost:8080/edu/listProgramId", String[].class));
		model.addAttribute("programId", programId);
		List<String> teacherId = Arrays.asList(rest.getForObject("http://localhost:8080/teacher/listTeacherId", String[].class));
		model.addAttribute("teacherId", teacherId);
		model.addAttribute("subject", subject);
		return "/EditSubject";
	}
	@PostMapping(value="/edit/{id1}/{id2}")
	public String saveSubject(@RequestParam("subjectId")String subjectId,@RequestParam("subjectName")String subjectName,@RequestParam("credit")String credit,
			@RequestParam("programId")String programId,@RequestParam("teacherId")String teacherId) {
		ProgramSubject s = new ProgramSubject(subjectId,subjectName,Integer.parseInt(credit),programId,teacherId);
		rest.postForObject("http://localhost:8080/subject", s, ProgramSubject.class);
		return "redirect:/subject"; 
		
	}
}
