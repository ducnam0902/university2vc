package universityfrontend.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class HomeController {
	private RestTemplate rest = new RestTemplate();
	@GetMapping
	public String home(Model model) {
		String teacher= rest.getForObject("http://localhost:8080/teacher/count",String.class);
		model.addAttribute("teacher", teacher);
		String student= rest.getForObject("http://localhost:8080/student/count",String.class);
		model.addAttribute("student", student);
		String falcuty= rest.getForObject("http://localhost:8080/falcuty/count",String.class);
		model.addAttribute("falcuty", falcuty);
		String edu= rest.getForObject("http://localhost:8080/edu/count",String.class);
		model.addAttribute("edu", edu);
		String subject= rest.getForObject("http://localhost:8080/subject/count",String.class);
		model.addAttribute("subject", subject);
		String scoring= rest.getForObject("http://localhost:8080/edu/count",String.class);
		model.addAttribute("scoring", scoring);
		return "Home";
	}
}
