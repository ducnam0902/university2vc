package universityfrontend.Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import universityfrontend.model.Learn;
import universityfrontend.model.ScoringStudent;
import universityfrontend.model.Student;

@Controller
@RequestMapping(value = "scoring")
public class ScoringController {
	private RestTemplate rest = new RestTemplate();

	@GetMapping
	public String showFormStudent(Model model) {
		return "selectStudent";
	}

	@GetMapping(value ="add/{id}")
	public String addNewScoring(Model model) {
		List<String> subjectName= Arrays.asList(rest.getForObject("http://localhost:8080/subject/allSubjectName", String[].class));
		model.addAttribute("subjectName",subjectName);
		return "AddNewScoring";
	}

	
	@PostMapping(value ="add/{id}") 
	public String addScoring(@PathVariable("id") String id,@RequestParam("subjectName")String subjectName,@RequestParam("score1")String score1,@RequestParam("score2")String score2,
			@RequestParam("score3")String score3,@RequestParam("score4")String score4,Model model) {
		String subjectId= rest.getForObject("http://localhost:8080/subject/subjectId/{id}", String.class,subjectName);
		Student s = rest.getForObject("http://localhost:8080/student/edit/{studentId}", Student.class, id);
		model.addAttribute("student", s);
		String programId= rest.getForObject("http://localhost:8080/subject/programId/{id}", String.class,subjectName);
		Learn learn2 = new Learn(id,subjectId,programId,Float.valueOf(score1),Float.valueOf(score2),Float.valueOf(score3),Float.valueOf(score4));
		rest.postForObject("http://localhost:8080/learn/", learn2, Learn.class);
		
		  List<Learn> learn = Arrays
		  .asList(rest.getForObject("http://localhost:8080/learn/student/{id}",
		  Learn[].class, id)); learn.get(0).getProgramId(); List<ScoringStudent>
		  scoreStudent = new ArrayList<ScoringStudent>(); int i; for (i = 0; i <
		  learn.size(); i++) { String subjectNam2e =
		  rest.getForObject("http://localhost:8080/subject/subjectName/{id}",
		  String.class, learn.get(i).getSubjectId()); ScoringStudent score = new
		  ScoringStudent(); score.setSubjectId(learn.get(i).getSubjectId());
		  score.setSubjectName(subjectNam2e);
		  score.setScore1(String.valueOf(learn.get(i).getScore_1()));
		  score.setScore2(String.valueOf(learn.get(i).getScore_2()));
		  score.setScore3(String.valueOf(learn.get(i).getScore_3()));
		  score.setMidScore(String.valueOf(learn.get(i).getMid_score())); float x =
		  (learn.get(i).getScore_1() + learn.get(i).getScore_2() +
		  learn.get(i).getScore_3() + learn.get(i).getMid_score() * 2) / 5;
		  score.setAverageScore(String.valueOf(x)); if (x >= 4.0)
		 score.setCondition("Đủ điều kiện"); else score.setCondition("Học lại");
		  scoreStudent.add(score); }
		  
		  model.addAttribute("score", scoreStudent);
		 
		return "ResultInformation";
	}
	 
	@PostMapping
	public String InformationStudent(Model model, @RequestParam("studentId") String studentId) {
		Student s = rest.getForObject("http://localhost:8080/student/edit/{studentId}", Student.class, studentId);
		if (s == null) {
			String mess = "No student with id " + studentId + " at the University";
			model.addAttribute("message", mess);
			return "selectStudent";
		} else {
			model.addAttribute("student", s);
			List<Learn> learn = Arrays
					.asList(rest.getForObject("http://localhost:8080/learn/student/{id}", Learn[].class, studentId));
			List<ScoringStudent> scoreStudent = new ArrayList<ScoringStudent>();
			int i;
			for (i = 0; i < learn.size(); i++) {
				String subjectName = rest.getForObject("http://localhost:8080/subject/subjectName/{id}", String.class,
						learn.get(i).getSubjectId());
				ScoringStudent score = new ScoringStudent();
				score.setSubjectId(learn.get(i).getSubjectId());
				score.setSubjectName(subjectName);
				score.setScore1(String.valueOf(learn.get(i).getScore_1()));
				score.setScore2(String.valueOf(learn.get(i).getScore_2()));
				score.setScore3(String.valueOf(learn.get(i).getScore_3()));
				score.setMidScore(String.valueOf(learn.get(i).getMid_score()));
				float x = (learn.get(i).getScore_1() + learn.get(i).getScore_2() + learn.get(i).getScore_3()
						+ learn.get(i).getMid_score() * 2) / 5;
				score.setAverageScore(String.valueOf(x));
				if (x >= 4.0)
					score.setCondition("Đủ điều kiện");
				else
					score.setCondition("Học lại");
				scoreStudent.add(score);
			}
				model.addAttribute("score", scoreStudent);
				return "ResultInformation";
			}
		
	}

	@GetMapping(value = "/edit/{id}/{id2}")
	public String findStudentId(@PathVariable("id") String id1, @PathVariable("id2") String id2, Model model) {
		Student s = rest.getForObject("http://localhost:8080/student/edit/{studentId}", Student.class, id1);
		model.addAttribute("student", s);
		String subjectName = rest.getForObject("http://localhost:8080/subject/subjectName/{id}", String.class, id2);
		Learn learn = rest.getForObject("http://localhost:8080/learn/stuandsub/{id1}/{id2}", Learn.class, id1, id2);
		ScoringStudent score = new ScoringStudent();
		score.setSubjectId(learn.getSubjectId());
		score.setSubjectName(subjectName);
		score.setScore1(String.valueOf(learn.getScore_1()));
		score.setScore2(String.valueOf(learn.getScore_2()));
		score.setScore3(String.valueOf(learn.getScore_3()));
		score.setMidScore(String.valueOf(learn.getMid_score()));
		model.addAttribute("score", score);
		return "EditScoring";
	}
	@PostMapping(value = "/edit/{id}/{id2}")
	public String updataStudentId(Model model,@PathVariable("id") String id1,@PathVariable("id2") String id2,@RequestParam("score1")String score1
			, @RequestParam("score2")String score2,@RequestParam("score3")String score3,@RequestParam("midScore")String midScore) {
		Learn x = rest.getForObject("http://localhost:8080/learn/stuandsub/{id1}/{id2}", Learn.class,id1,id2);
		Learn learn = new Learn(id1,id2,x.getProgramId(),Float.valueOf(score1),Float.valueOf(score2),Float.valueOf(score3),Float.valueOf(midScore));
		rest.postForObject("http://localhost:8080/learn", learn, Learn.class);
		Student s = rest.getForObject("http://localhost:8080/student/edit/{studentId}", Student.class, id1);
		model.addAttribute("student", s);
		List<Learn> scoreStudent = Arrays
				.asList(rest.getForObject("http://localhost:8080/learn/student/{id}", Learn[].class, id1));
		int i;
		List<ScoringStudent> scoreStudentId = new ArrayList<ScoringStudent>();
		for (i = 0; i < scoreStudent.size(); i++) {
			String subjectName = rest.getForObject("http://localhost:8080/subject/subjectName/{id}", String.class,
					scoreStudent.get(i).getSubjectId());
			ScoringStudent score = new ScoringStudent();
			score.setSubjectId(scoreStudent.get(i).getSubjectId());
			score.setSubjectName(subjectName);
			score.setScore1(String.valueOf(scoreStudent.get(i).getScore_1()));
			score.setScore2(String.valueOf(scoreStudent.get(i).getScore_2()));
			score.setScore3(String.valueOf(scoreStudent.get(i).getScore_3()));
			score.setMidScore(String.valueOf(scoreStudent.get(i).getMid_score()));
			float averageScore = (scoreStudent.get(i).getScore_1() + scoreStudent.get(i).getScore_2() + scoreStudent.get(i).getScore_3()
					+ scoreStudent.get(i).getMid_score() * 2) / 5;
			score.setAverageScore(String.valueOf(averageScore));
			if (averageScore >= 4.0)
				score.setCondition("Đủ điều kiện");
			else
				score.setCondition("Học lại");
			scoreStudentId.add(score);
		}
			model.addAttribute("score", scoreStudentId);
			return "ResultInformation";
	}

}
