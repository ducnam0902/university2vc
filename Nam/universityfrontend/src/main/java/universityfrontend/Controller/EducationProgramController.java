package universityfrontend.Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import universityfrontend.model.EducationProgram;

@Controller
@RequestMapping(value = "edu")
public class EducationProgramController {
	private RestTemplate rest = new RestTemplate();

	@GetMapping
	public String getAllEducationProgram(Model model) {
		List<EducationProgram> edu = Arrays
				.asList(rest.getForObject("http://localhost:8080/edu", EducationProgram[].class));
		List<String> falcutyName = new ArrayList<String>();
		int i;
		for(i=0;i<edu.size();i++)
			falcutyName.add(rest.getForObject("http://localhost:8080/edu/falcutyName/{id}", String.class,edu.get(i).getFalcutyId()));
		model.addAttribute("edu", edu);
		model.addAttribute("falcutyName", falcutyName);
		return "Education";
	}

	@GetMapping(value = "/add")
	public String addNewEducation(Model model) {
		List<String> listFalcuty = Arrays.asList(rest.getForObject("http://localhost:8080/edu/listid", String[].class));
		model.addAttribute("list", listFalcuty);
		return "AddEducation";
	}

	@PostMapping(value = "/add")
	public String addNewEducation(@RequestParam("programId") String id, @RequestParam("programName") String programName,
			@RequestParam("totalCredit") String totalCredit, @RequestParam("falcutyId") String falcutyId) {
		EducationProgram fa = new EducationProgram(id, programName, Integer.parseInt(totalCredit), falcutyId);
		rest.postForObject("http://localhost:8080/edu", fa, EducationProgram.class);
		return "redirect:/edu";

	}

	@GetMapping(value = "/delete/{id}")
	public String deleteEducation(@PathVariable("id") String id) {
		rest.delete("http://localhost:8080/edu/delete/{id}", id);
		return "redirect:/edu";
	}

	@GetMapping(value = "/edit/{id}")
	public String editEducation(@PathVariable("id") String id, Model model) {
		EducationProgram edu = rest.getForObject("http://localhost:8080/edu/edit/{id}", EducationProgram.class, id);
		model.addAttribute("edu", edu);
		List<String> listFalcuty = Arrays.asList(rest.getForObject("http://localhost:8080/edu/listid", String[].class));
		model.addAttribute("list", listFalcuty);
		return "/Editeducation";
	}

	@PostMapping(value = "/edit/{id}")
	public String saveFalcuty(@RequestParam("programId") String id, @RequestParam("programName") String programName,
			@RequestParam("totalCredit") String totalCredit, @RequestParam("falcutyId") String falcutyId) {
		EducationProgram edu = new EducationProgram(id, programName, Integer.parseInt(totalCredit), falcutyId);
		rest.postForObject("http://localhost:8080/edu", edu, EducationProgram.class);
		return "redirect:/edu";

	}
}
