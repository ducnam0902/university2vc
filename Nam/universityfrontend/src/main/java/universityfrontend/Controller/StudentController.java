package universityfrontend.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import universityfrontend.model.Student;

@Controller
@RequestMapping(value="stu")
public class StudentController {
	private RestTemplate rest = new RestTemplate();
	@GetMapping
	public String getAllStudent(Model model) {
		List<Student> stu = Arrays.asList(rest.getForObject("http://localhost:8080/student", Student[].class));
		model.addAttribute("stu", stu);
		return "Student";
	}
	@GetMapping(value="/add")
	public String addNewStudent() {
		return "AddStudent";
	}
	@PostMapping(value="/add")
	public String addNewStudent(@RequestParam("studentId")String studentId,@RequestParam("studentName")String studentName,@RequestParam("startedYear")String startedYear,
			@RequestParam("address")String address,@RequestParam("phoneNumber")String phoneNumber) {
		Student s= new Student(studentId,studentName,Integer.parseInt(startedYear),address,phoneNumber);
		rest.postForObject("http://localhost:8080/student",s, Student.class);
		return "redirect:/stu";
		
	}
	@GetMapping(value="/delete/{id}")
	public String deleteStudent(@PathVariable("id")String id) {
		rest.delete("http://localhost:8080/student/delete/{id}",id);
		return "redirect:/stu";
	}
	@GetMapping(value="/edit/{id}")
	public String editStudent(@PathVariable("id")String id,Model model) {
		Student stu= rest.getForObject("http://localhost:8080/student/edit/{id}",Student.class,id);
		model.addAttribute("stu", stu);
		return "/EditStudent";
	}
	@PostMapping(value="/edit/{id}")
	public String saveStudent(@RequestParam("studentId")String studentId,@RequestParam("studentName")String studentName,@RequestParam("startedYear")String startedYear,
			@RequestParam("address")String address,@RequestParam("phoneNumber")String phoneNumber) {
		Student s= new Student(studentName,studentId,Integer.parseInt(startedYear),address,phoneNumber);
		rest.postForObject("http://localhost:8080/student", s, Student.class);
		return "redirect:/stu"; 
		
	}
}
