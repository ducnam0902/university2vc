package universityfrontend.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import universityfrontend.model.Falcuty;

@Controller
@RequestMapping(value="falcuty")
public class FalcutyController {
	private RestTemplate rest = new RestTemplate();
	@GetMapping
	public String getAllFalcuty(Model model) {
		List<Falcuty> falcutys = Arrays.asList(rest.getForObject("http://localhost:8080/falcuty", Falcuty[].class));
		model.addAttribute("falcuty", falcutys);
		return "Falcuty";
	}
	@GetMapping(value="/add")
	public String addNew() {
		return "AddFalcuty";
	}
	@PostMapping(value="/add")
	public String addNewFalcuty(@RequestParam("id")String id,@RequestParam("name")String name,@RequestParam("add")String add,
			@RequestParam("phone")String phone,Model model) {
		Falcuty fa = new Falcuty(id, name, add, phone);
		rest.postForObject("http://localhost:8080/falcuty", fa, Falcuty.class);
		return "redirect:/falcuty";
		}
	@GetMapping(value="/delete/{id}")
	public String deleteFalcuty(@PathVariable("id")String id) {
		rest.delete("http://localhost:8080/falcuty/delete/{id}",id);
		return "redirect:/falcuty";
	}
	@GetMapping(value="/edit/{id}")
	public String editFalcuty(@PathVariable("id")String id,Model model) {
		Falcuty fal= rest.getForObject("http://localhost:8080/falcuty/edit/{id}",Falcuty.class,id);
		model.addAttribute("falcuty", fal);
		return "/EditFalcuty";
	}
	@PostMapping(value="/edit/{id}")
	public String saveFalcuty(@RequestParam("id")String id,@RequestParam("name")String name,@RequestParam("add")String add,
			@RequestParam("phone")String phone) {
		Falcuty fa= new Falcuty(id,name,add,phone);
		rest.postForObject("http://localhost:8080/falcuty", fa, Falcuty.class);
		return "redirect:/falcuty"; 
		
	}
}
