package universityfrontend.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import universityfrontend.model.Teacher;

@Controller
@RequestMapping(value="teacher")
public class TeacherController {
	private RestTemplate rest = new RestTemplate();
	@GetMapping
	public String getAllTeacher(Model model) {
		List<Teacher> teacher = Arrays.asList(rest.getForObject("http://localhost:8080/teacher", Teacher[].class));
		model.addAttribute("teacher", teacher);
		return "Teacher";
	}
	@GetMapping(value="/add")
	public String addNewTeacher() {
		return "AddTeacher";
	}
	@PostMapping(value="/add")
	public String addNewTeacher(@RequestParam("teacherId")String teacherId,@RequestParam("teacherName")String teacherName,@RequestParam("seniority")String seniority) {
		Teacher t = new Teacher(teacherId,teacherName,Integer.parseInt(seniority));
		rest.postForObject("http://localhost:8080/teacher", t, Teacher.class);
		return "redirect:/teacher";
		
	}
	@GetMapping(value="/delete/{id}")
	public String deleteTeacher(@PathVariable("id")String id) {
		rest.delete("http://localhost:8080/teacher/delete/{id}",id);
		return "redirect:/teacher";
	}
	@GetMapping(value="/edit/{id}")
	public String editTeacher(@PathVariable("id")String id,Model model) {
		Teacher t= rest.getForObject("http://localhost:8080/teacher/edit/{id}",Teacher.class,id);
		model.addAttribute("teacher", t);
		return "/EditTeacher";
	}
	@PostMapping(value="/edit/{id}")
	public String saveTeacher(@RequestParam("teacherId")String teacherId,@RequestParam("teacherName")String teacherName,@RequestParam("seniority")String seniority) {
		Teacher t = new Teacher(teacherId,teacherName,Integer.parseInt(seniority));
		rest.postForObject("http://localhost:8080/teacher", t, Teacher.class);
		return "redirect:/teacher"; 
	}
}
