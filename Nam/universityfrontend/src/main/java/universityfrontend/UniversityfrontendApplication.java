package universityfrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversityfrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversityfrontendApplication.class, args);
	}

}
